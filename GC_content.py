# Skript zum Quantifizieren von GC content

import sys

seq = str(sys.argv[1])
namensdatei = str(sys.argv[2])

with open (seq, "r") as f:
    sequenz = f.read()

with open (namensdatei, "r") as f:
    name = f.read()

total_counter = 0
GC_counter = 0

for base in sequenz:
    if base == "C" or base == "G":
        GC_counter += 1
    if base in "CGAT":
        total_counter += 1

GC_content = GC_counter/total_counter *100


with open ("GC_content.txt", "a") as datei:
    datei.write(f"GC-content von {name} ist {GC_content}")
