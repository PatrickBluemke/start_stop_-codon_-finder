nextflow.enable.dsl=2

process split_file {
  input:
    path infile
  output:
    path "${infile}.line*"
  script:
    """
    split -l 2 ${infile} -d ${infile}.line
    """
}

process GC_content {
  publishDir params.outdir, mode: 'copy', overwrite: true
  input:
    path infile
  output:
    path "${infile}.GC_content", emit: GC_content
  script:
    """
    head -n 1 ${infile} > name
    tail -n 1 ${infile} > sequenz
    
    python3 /home/paddy/Bioinformatik/NextGenerationSeq/start_stop_-codon_-finder/GC_content.py sequenz name > ${infile}.GC_content

    """
}


workflow {
  if(!file(params.infile).exists()) {
    println("Input file ${params.infile} does not exist.")
    exit(1)
  }

  inchannel = channel.fromPath(params.infile)
  splitfiles = split_file(inchannel)
  GC_content = GC_content(splitfiles.flatten())

}



