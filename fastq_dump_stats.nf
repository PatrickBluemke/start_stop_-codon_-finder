nextflow.enable.dsl = 2

params.with_stats = false
params.with_fastqc = false
params.with_fastp = false
params.unzip = false


process prefetch {
  storeDir "${params.outdir}" //statt publishDir, prüft erst, ob output evtl schon vorhanden
  container "https://depot.galaxyproject.org/singularity/sra-tools%3A2.11.0--pl5262h314213e_0"
  input: 
    val accession
  output:
    path "${accession}/${accession}.sra", emit: sra_file 
  script:
    """
    prefetch $accession
    """
}

process fastq_dump {
  storeDir "${params.outdir}" 
  container "https://depot.galaxyproject.org/singularity/sra-tools%3A2.11.0--pl5262h314213e_0"
  input: 
    val accession
    path sra_download
  output:
    path "${accession}_*.fastq", emit: fastq 
  script:
    """
    fastq-dump --split-files $sra_download
    """
}

process ngsutil_stats {
  storeDir "${params.outdir}" 
  container "https://depot.galaxyproject.org/singularity/ngsutils%3A0.5.9--py27heb79e2c_4"
  input: 
    path fastq_file
  output:
    path "${fastq_file.getSimpleName()}.txt", emit: statistics 
  script:
    """
    fastqutils stats ${fastq_file} > ${fastq_file.getSimpleName()}.txt
    
    """
}

//https://depot.galaxyproject.org/singularity/r-fastqcr%3A0.1.2--r351h6115d3f_0


process fastqc {
  storeDir "${params.outdir}/${params.accession}_stats" 
  container "https://depot.galaxyproject.org/singularity/fastqc:0.11.9--0"
  input: 
    path fastq_file
  output:
    path "*fastqc.html", emit: html_files
    path "*fastqc.zip", emit: zip_files
  script:
    """
    fastqc ${fastq_file} #alternativ alle fastqc Dateien in einen ordner (vorher erstellen: mkdir ./fastQC_results)
                         # mit: fastqc -o ./fastQC_results ${fastq_file} und dann als output den ordner angeben fastQC_results/*
    
    """
}

process unzip {
  publishDir "${params.outdir}/${params.accession}_stats", mode: "copy", overwrite: true 
  input: 
    path zip_files
  output:
    path "*fastqc"
  script:
    """
    unzip -e ${zip_files}
    
    """
}

process fastp {
  publishDir "${params.outdir}/${params.accession}_stats", mode: "copy", overwrite: true 
  container "https://depot.galaxyproject.org/singularity/fastp%3A0.22.0--h2e03b76_0"
  input:
    path unprocessed_fastq
  output:
    path "*_trim.fastq", emit: fastq_trimmed
    path "*json", emit: fastp_json
    path "*html", emit: fastp_html
  script:
    """
    fastp -i ${unprocessed_fastq} -o ${unprocessed_fastq.getSimpleName()}_trim.fastq
    mv fastp.json ${unprocessed_fastq.getSimpleName()}_fastp.json
    mv fastp.html ${unprocessed_fastq.getSimpleName()}_fastp.html
    
    """
}

process multiqc {
  publishDir "${params.outdir}/${params.accession}_stats", mode: "copy", overwrite: true 
  container "https://depot.galaxyproject.org/singularity/multiqc%3A1.9--pyh9f0ad1d_0"
  input:
    path input_folder
  output:
    path "*_report.html", emit: report

  script:
    """
    multiqc .
    """
}

//# -d  -n ${params.accession}_mulitqc_report

workflow {
  sra_download = prefetch(params.accession)
  fastq_files_pre = fastq_dump(params.accession, sra_download)
  
  if (params.with_fastp) {
      fastp_processed = fastp(fastq_files_pre.flatten())
      fastp_fastq_dateien = fastp_processed.fastq_trimmed
      fastp_json_dateien = fastp_processed.fastp_json
      
  } else {
    fastp_processed = channel.empty()
  }
  
  if (params.with_stats) {
      
      pre_and_after = fastq_files_pre.concat(fastp_fastq_dateien)
      ngsutil_stats(pre_and_after.flatten())
  } 
  
  if (params.with_fastqc) {
  
      pre_and_after = fastq_files_pre.concat(fastp_fastq_dateien)
      fastqc_channel = fastqc(pre_and_after.flatten())
      if (params.unzip) {
        unzip(fastqc_channel.zip_files.flatten())
      }
  }
  
  multiQC_channel = multiqc(fastp_json_dateien.collect()) //.concat(fastp_json_dateien).collect())
  //multiqc(fastqc_channel.collect()) //.concat(fastp_json_dateien)).collect())
  //generate_report = multiqc(multiqc_input)

  //if (params.with_multiqc) {
      
  
  //}
}
