import sys

fasta_datei = sys.argv[1]

with open (fasta_datei, "r") as datei:
    name = datei.readline()[:-1]
    seq = datei.readline()[:-1]


count = 0
for base in seq:
    if base in "ATGC":
        count += 1


print(f"{name} \n{seq} \ntotal base count: {count}")
