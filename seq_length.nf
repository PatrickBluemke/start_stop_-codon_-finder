nextflow.enable.dsl = 2

process splitFile{
  input:
    path infile
  output:
    path "${infile}.seq*"
  script:
    """
    split -l 2 ${infile} -d ${infile}.seq
    """
}

process count_length{
  input:
    path splitted_files
  output:
    path "${splitted_files}.seqlength"
  script:
    """
    head -n 1 ${splitted_files} > name
    tail -n 1 ${splitted_files} > sequence
    cat name > ${splitted_files}.seqlength
    cat sequence | tr -d "\n" | wc -m >> ${splitted_files}.seqlength #tr translate -d delete Zeilenumbruch
    """
}

process all_lengths {
  publishDir params.outdir, mode: 'copy', overwrite: true
  input:
    path inpairs
  output:
    path "all_lengths"
  script:
    """
    cat ${inpairs} > all_lengths 

    """
  
}

workflow {
  if(!file(params.infile).exists()) {
    println("Input file ${params.infile} does not exist.")
    exit(1)
  }

  inchannel = channel.fromPath(params.infile)
  splitted_files = splitFile(inchannel)
  single_length = count_length(splitted_files.flatten())
  all_lengths(single_length.collect())

}



