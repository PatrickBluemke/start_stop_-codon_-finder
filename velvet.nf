nextflow.enable.dsl = 2

params.with_velvet = false
params.with_spades = false

process prefetch {
  storeDir "${params.outdir}" //statt publishDir, prüft erst, ob output evtl schon vorhanden
  container "https://depot.galaxyproject.org/singularity/sra-tools%3A2.11.0--pl5262h314213e_0"
  input: 
    val accession
  output:
    path "${accession}/${accession}.sra", emit: sra_file 
  script:
    """
    prefetch $accession
    """
}

process fastq_dump {
  storeDir "${params.outdir}" 
  container "https://depot.galaxyproject.org/singularity/sra-tools%3A2.11.0--pl5262h314213e_0"
  input: 
    val accession
    path sra_download
  output:
    path "${accession}_*.fastq", emit: fastq 
  script:
    """
    fastq-dump --split-files  -e $sra_download
    """
}

process velveth{
  publishDir "${params.outdir}/${params.accession}", mode: "copy", overwrite: true 
  container "https://depot.galaxyproject.org/singularity/velvet%3A1.2.10--h5bf99c6_4"
  input:
    val kmerlen
    path fastq
    val cov_cutoff
  output:
    path "out_h"
    path "out_h/*contigs.fa", emit: velvet_contig
  script:
    """
    velveth out_h/ ${kmerlen} -fastq -short ${fastq} #alternativ: shortPaired 
    velvetg out_h/ -cov_cutoff ${cov_cutoff} -min_contig_lgth 2 
    mv out_h/contigs.fa out_h/${params.accession}_velvet_contigs.fa
    """
}


/*
process velvetg{
  storeDir "${params.outdir}/${params.accession}" 
  container "https://depot.galaxyproject.org/singularity/velvet%3A1.2.10--h5bf99c6_4"
  input:
    val cov_cutoff
    path out_h
  output:
    path "out_g/*"
  script:
    """
    
    velvetg ${out_h} -cov_cutoff ${cov_cutoff} -min_contig_lgth 2
    
    """
}
*/


process spades {
  storeDir "${params.outdir}/${params.accession}" 
  container "https://depot.galaxyproject.org/singularity/spades%3A3.9.1--h9ee0642_1"
  input:
    path fastq
  output:
    path "*"
    path "spades/*contigs.fa", emit: spades_contig
  script:
    """
     spades.py -s ${fastq} -o spades/
     mv spades/contigs.fasta spades/${params.accession}_spades_contigs.fa
     
    """
}

process quast {
  storeDir "${params.outdir}" 
  container "https://depot.galaxyproject.org/singularity/quast%3A5.0.2--py36pl5262h30a8e3e_4"
  input:
    path contigs_files
  output:
    path "quast_results"

  script:
    """
    quast.py ${contigs_files}
    
    """

}

workflow {
    download = prefetch(params.accession)
    fastq = fastq_dump(params.accession, download)
    
    if (params.with_velvet){ 
        outchannel_velveth = velveth(params.kmerlen, fastq, params.cov_cutoff)
        //outchannel_velvetg = velvetg(params.cov_cutoff, outchannel_velveth.collect())
    }
    
    if (params.with_spades){
        outchannel_spades = spades(fastq)
    }
    
    inchannel_quast = outchannel_velveth.velvet_contig.concat(outchannel_spades.spades_contig)
    //inchannel_quast.collect().view()
    quast(inchannel_quast.collect())
}
